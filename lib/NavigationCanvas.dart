import 'dart:async';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:nav_bot/PointerPainter.dart';
import 'dart:convert' as convert;
import 'package:flutter_compass/flutter_compass.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:geolocator/geolocator.dart';
import 'package:sensors/sensors.dart';
import 'package:nav_bot/HttpInterface.dart';

// Define a custom Form widget.
class NavigationCanvasWidget extends StatefulWidget {
  NavigationCanvasWidget({Key key}) : super(key: key);
  @override
  NavigationCanvas createState() => NavigationCanvas ();
}

class NavigationCanvas extends State<NavigationCanvasWidget> {
  // Initiate the interface communication, chatbot parameter indicating which channel to activate to the feature-related microservice
  HttpInterface httpInterface = new HttpInterface("navigation");

  // Var related for foreground/background purpose
  bool _disposed = false;
  DateTime time = DateTime.now();

  List<StreamSubscription<dynamic>> _streamSubscriptions = <StreamSubscription<dynamic>>[];

  // Variables related to GPS
  // Object necessary for GPS-related things
  var geolocator = Geolocator();
  // Option for high accuracy and low distance filter, so we can detect each move
  var locationOptions = LocationOptions(accuracy: LocationAccuracy.high, distanceFilter: 1);
  // Values taken by the GPS
  Position initialGpsPositionValue;

  // Accelerometers values
  List<double> _accelerometerValues;
  //final List<String> accelerometer = _accelerometerValues?.map((double v) => v.toStringAsFixed(1))?.toList();
  List<String> accelerometer;
  // Variables related to compass
  // We admit that user doesn't has permission
  bool _hasPermissions = false;
  double _lastRead = 0;
  DateTime _lastReadAt;

  @override
  void initState() {
    // TODO: implement initState
    Timer(Duration(seconds: 1), () {
      if (!_disposed)
        setState(() {
          time = time.add(Duration(seconds: -1));
        });
    });
    super.initState();

    WidgetsBinding.instance.addPostFrameCallback((_) => getDeviceInitialPosition());

    _streamSubscriptions.add(accelerometerEvents.listen((AccelerometerEvent event) {
      setState(() {
        _accelerometerValues = <double>[event.x, event.y, event.z];
        //accelerometer = _accelerometerValues?.map((double v) => v.toStringAsFixed(1))?.toList();
        print('Accelerometer : x = ${event.x}, y = ${event.y}, z = ${event.z}');
        //print('Accelerometer : x = ${accelerometer[0]}, y = ${accelerometer[1]}, z = ${accelerometer[2]}');
      });
    }));

    _streamSubscriptions.add(FlutterCompass.events.listen((event) {
      setState(() {
        print("ORIENTATION " + event.toString());

      });
    }));

    /*
    accelerometerEvents.listen((AccelerometerEvent event) {
      setState(() {
        print(event);
      });
    });
     */

    /*
    FlutterCompass.events.listen((event) {
      setState(() {
        print(event.toString());

      });
    });

     */
  }

  @override
  Widget build(BuildContext context) {
    final List<String> accelerometer =
    _accelerometerValues?.map((double v) => v.toStringAsFixed(1))?.toList();

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      //theme: ThemeData.dark().copyWith(scaffoldBackgroundColor: darkBlue),
      home: Scaffold(
        // Outer white container with padding
        body: Builder(builder: (context) {
          if (_hasPermissions) {
            return Container(
              //padding: const EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
              color: Colors.white,
              // Inner yellow container
              child: StreamBuilder(
                stream: FlutterCompass.events,
                builder: (context, snapshot) {
                  Container innerCanvasContainer;
                  /*
                  if (snapshot.hasError) {
                    // return Text('Error reading heading: ${snapshot.error}');
                    print('Error reading heading: ${snapshot.error}');
                  }

                  if (snapshot.connectionState == ConnectionState.waiting) {
                    print("waiting");
                    /*
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                    */
                  }

                  double direction = snapshot.data;

                  // if direction is null, then device does not support this sensor
                  // show error message
                  if (direction == null) {
                    print("Device does not have sensors !");
                    /*
                    return Center(
                      child: Text("Device does not have sensors !"),
                    );

                     */
                  }

                   */


                  innerCanvasContainer = Container(
                    color: Colors.black,
                    width: double.infinity,
                    height: double.infinity,
                    //child: CustomPaint(painter: PointerPainter()),
                    child: CustomPaint(
                        size: Size(double.infinity, double.infinity),
                        painter: PointerPainter()),
                  );

                  return innerCanvasContainer;
                  // Text(snapshot.hasData ? '${snapshot.data}' : '');
                },
              ),
            );
            /*
            return Column(
              children: <Widget>[
                _buildManualReader(),
                Expanded(child: _buildCompass()),
              ],
            );

             */
          } else {
            return _buildPermissionSheet();
          }
        }),
      ),
    );
  }

  Widget _buildPermissionSheet() {
    return Center(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Text('Location Permission Required'),
          RaisedButton(
            child: Text('Request Permissions'),
            onPressed: () {
              PermissionHandler().requestPermissions(
                  [PermissionGroup.locationWhenInUse]).then((ignored) {
                _fetchPermissionStatus();
              });
            },
          ),
          SizedBox(height: 16),
          RaisedButton(
            child: Text('Open App Settings'),
            onPressed: () {
              PermissionHandler().openAppSettings().then((opened) {
                //
              });
            },
          )
        ],
      ),
    );
  }

  void _fetchPermissionStatus() {
    PermissionHandler()
        .checkPermissionStatus(PermissionGroup.locationWhenInUse)
        .then((status) {
      if (mounted) {
        setState(() => _hasPermissions = status == PermissionStatus.granted);
      }
    });
  }

  void getDeviceInitialPosition() async {
    initialGpsPositionValue = await Geolocator().getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
    print("Initial Position : " + initialGpsPositionValue.latitude.toString() + " " + initialGpsPositionValue.longitude.toString());
  }

  @override
  void dispose() {
    _disposed = true;
    super.dispose();
    for (StreamSubscription<dynamic> subscription in _streamSubscriptions) {
      subscription.cancel();
    }
  }
}