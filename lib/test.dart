import 'dart:async';
//import 'dart:html';
import 'package:flutter/material.dart';
import 'package:nav_bot/HttpInterface.dart';
import 'package:sensors/sensors.dart';
import 'package:geolocator/geolocator.dart';
//import 'package:flutter_magnetometer/flutter_magnetometer.dart';

class Test extends StatefulWidget {
  Test({Key key}) : super(key: key);
  @override
  _TestState createState() => new _TestState();
}

class _TestState extends State<Test> {

  List<double> _accelerometerValues;
  List<double> _userAccelerometerValues;
  List<double> _gyroscopeValues;
  List<double> _gpsValues;
  List<double> _magnetometerValues;
  var geolocator = Geolocator();
  var locationOptions = LocationOptions(accuracy: LocationAccuracy.high, distanceFilter: 1);
  List<StreamSubscription<dynamic>> _streamSubscriptions = <StreamSubscription<dynamic>>[];
  // Magnetometer magnetometerVar = new Magnetometer();
  //HttpInterface httpInterface = new HttpInterface();

  @override
  Widget build(BuildContext context) {
    final List<String> accelerometer = _accelerometerValues?.map((double v) => v.toStringAsFixed(1))?.toList();
    final List<String> gyroscope = _gyroscopeValues?.map((double v) => v.toStringAsFixed(1))?.toList();
    final List<String> gps = _gpsValues?.map((double v) => v.toStringAsFixed(1))?.toList();
    final List<String> magnetometer = _magnetometerValues?.map((double v) => v.toStringAsFixed(1))?.toList();
    //final List<String> userAccelerometer = _userAccelerometerValues?.map((double v) => v.toStringAsFixed(1))?.toList();

    return new Scaffold(
      appBar: new AppBar(
        title: new Text('App Name'),
      ),
      body:
      new Container(
        child:
        new Column(
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              new Padding(
                child:
                new Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      new Text(
                        'Accelerometer : x = ${accelerometer[0]}, y = ${accelerometer[1]}, z = ${accelerometer[2]}',
                        style: new TextStyle(fontSize:12.0,
                            color: const Color(0xFF000000),
                            fontWeight: FontWeight.w200,
                            fontFamily: "Roboto"),
                      ),
                    ]

                ),

                padding: const EdgeInsets.all(24.0),
              ),
              new Divider(color: const Color(0x5e5e5e)),
              new Padding(
                child:
                new Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      new Text(
                        'Gyroscope : x = ${gyroscope[0]}, y = ${gyroscope[1]}, z = ${gyroscope[2]}',
                        style: new TextStyle(fontSize:12.0,
                            color: const Color(0xFF000000),
                            fontWeight: FontWeight.w200,
                            fontFamily: "Roboto"),
                      ),
                    ]

                ),

                padding: const EdgeInsets.all(24.0),
              ),
              new Divider(color: const Color(0x5e5e5e)),
              new Padding(
                child:
                new Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      new Text(
                        "Magnetometer : $magnetometer",
                        style: new TextStyle(fontSize:12.0,
                            color: const Color(0xFF000000),
                            fontWeight: FontWeight.w200,
                            fontFamily: "Roboto"),
                      ),

                      new Text(
                        "x =",
                        style: new TextStyle(fontSize:12.0,
                            color: const Color(0xFF000000),
                            fontWeight: FontWeight.w200,
                            fontFamily: "Roboto"),
                      ),
                      new Text(
                        "qWerty1",
                        style: new TextStyle(fontSize:12.0,
                            color: const Color(0xFF000000),
                            fontWeight: FontWeight.w200,
                            fontFamily: "Roboto"),
                      ),
                      new Text(
                        "y =",
                        style: new TextStyle(fontSize:12.0,
                            color: const Color(0xFF000000),
                            fontWeight: FontWeight.w200,
                            fontFamily: "Roboto"),
                      ),
                      new Text(
                        "qWerty1",
                        style: new TextStyle(fontSize:12.0,
                            color: const Color(0xFF000000),
                            fontWeight: FontWeight.w200,
                            fontFamily: "Roboto"),
                      ),new Text(
                        "z =",
                        style: new TextStyle(fontSize:12.0,
                            color: const Color(0xFF000000),
                            fontWeight: FontWeight.w200,
                            fontFamily: "Roboto"),
                      ),
                      new Text(
                        "qWerty1",
                        style: new TextStyle(fontSize:12.0,
                            color: const Color(0xFF000000),
                            fontWeight: FontWeight.w200,
                            fontFamily: "Roboto"),
                      )
                    ]

                ),

                padding: const EdgeInsets.all(24.0),
              ),
              new Divider(color: const Color(0x5e5e5e)),
              new Padding(
                child:
                new Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      new Text(
                        'GPS : Latitude = ${gps[0]}, Longitude = ${gps[1]}',
                        style: new TextStyle(fontSize:12.0,
                            color: const Color(0xFF000000),
                            fontWeight: FontWeight.w200,
                            fontFamily: "Roboto"),
                      ),
                    ]

                ),

                padding: const EdgeInsets.all(24.0),
              ),
              new Divider(color: const Color(0x5e5e5e)),
              new Padding(
                child:
                new Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      new Text(
                        "Quelque chose :",
                        style: new TextStyle(fontSize:12.0,
                            color: const Color(0xFF000000),
                            fontWeight: FontWeight.w200,
                            fontFamily: "Roboto"),
                      ),

                      new Text(
                        "x =",
                        style: new TextStyle(fontSize:12.0,
                            color: const Color(0xFF000000),
                            fontWeight: FontWeight.w200,
                            fontFamily: "Roboto"),
                      ),
                      new Text(
                        "qWerty1",
                        style: new TextStyle(fontSize:12.0,
                            color: const Color(0xFF000000),
                            fontWeight: FontWeight.w200,
                            fontFamily: "Roboto"),
                      ),
                      new Text(
                        "y =",
                        style: new TextStyle(fontSize:12.0,
                            color: const Color(0xFF000000),
                            fontWeight: FontWeight.w200,
                            fontFamily: "Roboto"),
                      ),
                      new Text(
                        "qWerty1",
                        style: new TextStyle(fontSize:12.0,
                            color: const Color(0xFF000000),
                            fontWeight: FontWeight.w200,
                            fontFamily: "Roboto"),
                      ),new Text(
                        "z =",
                        style: new TextStyle(fontSize:12.0,
                            color: const Color(0xFF000000),
                            fontWeight: FontWeight.w200,
                            fontFamily: "Roboto"),
                      ),
                      new Text(
                        "qWerty1",
                        style: new TextStyle(fontSize:12.0,
                            color: const Color(0xFF000000),
                            fontWeight: FontWeight.w200,
                            fontFamily: "Roboto"),
                      )
                    ]

                ),

                padding: const EdgeInsets.all(24.0),
              )
            ]

        ),

        padding: const EdgeInsets.all(0.0),
        alignment: Alignment.center,
      ),

    );
  }

  @override
  void dispose() {
    super.dispose();
    for (StreamSubscription<dynamic> subscription in _streamSubscriptions) {
      subscription.cancel();
    }
  }
  
  @override
  void initState() {
    super.initState();
    _streamSubscriptions.add(accelerometerEvents.listen((AccelerometerEvent event) {
      setState(() {
        _accelerometerValues = <double>[event.x, event.y, event.z];
      });
    }));
    _streamSubscriptions.add(gyroscopeEvents.listen((GyroscopeEvent event) {
      setState(() {
        _gyroscopeValues = <double>[event.x, event.y, event.z];
        //debugPrint('movieTitle: '+httpInterface.openPersistentConnection());
        //httpInterface.openPersistentConnection();
      });
    }));
    _streamSubscriptions.add(userAccelerometerEvents.listen((UserAccelerometerEvent event) {
      setState(() {
        _userAccelerometerValues = <double>[event.x, event.y, event.z];
      });
    }));

    /*
    _streamSubscriptions.add(FlutterMagnetometer.events
        .listen((MagnetometerData data) {
      setState(() {
        _magnetometerValues= <double>[data.x, data.y, data.z];
      });
    }));

     */
    /*
    _magnetometerListener = FlutterMagnetometer.events
        .listen((MagnetometerData data) => setState(() => _magnetometerData = data));
        */
    /*
    _streamSubscriptions.add(magnetometerVar.on{
      setState(() {
        _userAccelerometerValues = <double>[event.x, event.y, event.z];
      });
    }));
    */
    //_streamSubscriptions.add(magnetometerVar.x);
    _streamSubscriptions.add(geolocator.getPositionStream(locationOptions).listen((Position position) {
      setState(() {
        if (position == null) {
          _gpsValues = <double>[0, 0];
        } else {
          _gpsValues = <double>[position.latitude, position.longitude];
        }
        //_gpsValues = position == null ? <double>[0, 0] :<double>[position.latitude, position.longitude];
      });
    }));
        /*
          print(position == null ? 'Unknown' : position.latitude.toString() + ', ' + position.longitude.toString());
        }));
        */
    /*
      StreamSubscription<Position> positionStream = geolocator.getPositionStream(locationOptions).listen(
              (Position position) {
            print(position == null ? 'Unknown' : position.latitude.toString() + ', ' + position.longitude.toString());
          });
      */
    //Position position = await Geolocator().getLastKnownPosition(desiredAccuracy: LocationAccuracy.high);
  }

}