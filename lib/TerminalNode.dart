import 'dart:async';
import 'dart:convert' as convert;

class TerminalNode {
  String name;
  int id;

  TerminalNode(String name, int id) {
    this.name = name;
    this.id = id;
  }

  String toString() {
    return 'TerminalNode{' +
        'name=' + name +
        ', id=' + id.toString() +
        '}';
  }

  Map<String, dynamic> toJson() =>
      {
        'name' : name,
        'id': id,
      };

  TerminalNode.fromJson(Map<String, dynamic> jsonParam) :
        name = jsonParam['name'],
        id = jsonParam['id'];
}