import 'package:flutter/material.dart';
import 'package:bubble/bubble.dart';
import 'package:nav_bot/HttpInterface.dart';
import 'package:nav_bot/Message.dart';
import 'dart:async';
import 'dart:convert' as convert;
import 'package:flutter_downloader/flutter_downloader.dart';

// Define a custom Form widget.
class ChatbotViewWidget extends StatefulWidget {
  ChatbotViewWidget({Key key}) : super(key: key);
  @override
  ChatbotView createState() => ChatbotView ();
}

class ChatbotView extends State<ChatbotViewWidget> {
  var bubblesWidgets = new List<Bubble>();
  var messages = List<Message>();
  Future<Message> futureMessage;
  HttpInterface httpInterface = new HttpInterface("chatbot");
  final TextEditingController _textFieldController = TextEditingController();
  //var eventBus;
  var _log;

/*
  @override
  void didChangeDependencies() {
    //futureMessage = Provider.of<Message>(context, listen: true);
    //customListener.setEventBus(this.eventBus);
    //print(customListener.eventBus);
    /*
    debugPrint('testDidChangedependencies');
    messageEventListener.listenForMessageEvent();

     */
    super.didChangeDependencies();
    //futureMessage = httpInterface.openPersistentConnection("testDidChangedependencies");
  }

 */

  @override
  void initState() {
    super.initState();
    httpInterface.initFlutterDownload();
    //print("INITSTATE");
    //messageEventListener = MessageEventListener();
    //futureMessage = httpInterface.openPersistentConnection();
    // httpInterface.openWebSocket();
    //httpInterface.listenForServersMessage(f)
  }

  @override
  Widget build(BuildContext context) {
    /*
    print("toctoc");
    _log = Logger('event_bus_example');
    initLogging();
    */
    //eventBus = Provider.of<EventBus>(context);

    return Scaffold(
      appBar: AppBar(
        title: Text("Chatbot"),
      ),
      backgroundColor: const Color(0xFF424242),

      body: new Stack(
        //alignment:new Alignment(x, y)
        children: <Widget>[
          new Positioned(
            child: Padding(
              padding: EdgeInsets.all(16.0),
              child: /*
              ListView(
                padding: const EdgeInsets.all(8),
                children: bubblesWidgets,
              )
*/
              new SingleChildScrollView(
                child : StreamBuilder(
                  stream: httpInterface.channel.stream,
                  builder: (context, snapshot) {
                    // BUG DU DOUBLE SNAPSHOT RECU EN DEPLACANT CETTE INSTRUCTION DE SON EMPLACEMENT D'ORIGINE, CAD LE SETSTATE DANS LA METHODE ONCLEAR !!!!!!
                    //_textFieldController.text = "";
                    Column column;
                      if (snapshot.hasData) {
                        Message message = new Message(false, DateTime.now().toString(), snapshot.data.toString());
                        print("Space machin :" + snapshot.data.toString());
                        //messages.add(message);
                        if(snapshot.data.toString() == "close") {
                          httpInterface.channel.sink.close();
                          print("closed");
                        } else {
                          addBotBubble(message);
                        }
                      }
                      column = new Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        mainAxisSize: MainAxisSize.max,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: bubblesWidgets,
                        //]
                      );

                    return column;
                    // Text(snapshot.hasData ? '${snapshot.data}' : '');
                  },
                ),
              )

            ),
          ),
          new Positioned(
            child: new Align(
                alignment: FractionalOffset.bottomCenter,
                child: new Container (
                    child:
                    Padding(
                      padding: EdgeInsets.all(16.0),
                      child: TextField(
                        controller: _textFieldController,
                        style: new TextStyle(color: Colors.white),
                        /*
                        decoration: InputDecoration(
                          icon: Icon(Icons.send),
                          //hintText: 'Hint Text',
                          //helperText: 'Helper Text',
                          //counterText: '0 characters',
                          //border: const OutlineInputBorder(),
                          border: InputBorder.none,
                          hintStyle: TextStyle(fontSize: 12.0, color: Colors.white),
                          hintText: 'Enter a search term'
                        ),

                         */
                        /*
                        decoration: InputDecoration(
                          //hintStyle: TextStyle(fontSize: 12.0, color: Colors.white),
                          //hintText: 'Enter a search term',

                          suffixIcon: IconButton(
                            icon: Icon(Icons.send),
                            color: const Color(0xFF5facdf),
                            onPressed: onClear(_textFieldController.text),
                          ),


                        ),

                         */
                      ),
                    ),
                ),
            ),
          ),

          new IconButton(
            icon: const Icon(Icons.send),
            onPressed: onClear,
            iconSize: 48.0,
            color: const Color(0xFF439cd7),
          ),


        ],
      ),
    );
  }

  addMessage(String text, var dateTime,bool isHumanParameter) {

    //MyEventB myEventB = MyEventB('Mickey');
    //messageEventListener.eventBus.fire(MessageEvent(text, dateTime, isHumanParameter));
    //eventBus.on().listen((event) => _log.finest('event fired:  ${event.runtimeType}'));
    Message messageToAdd = new Message(isHumanParameter, Null,text);
    messages.add(messageToAdd);
    addHumanBubble(messages.last);

    //print("MessageContent : " + messageToAdd.messageContent);
    //Future<Message> messageToGet = httpInterface.openPersistentConnection(messageToAdd.messageContent);
    httpInterface.writeMessageToSocket(messages.last);

    if (messages.last.messageContent == "download") {
      print("downodnadad");
      httpInterface.downloadFile();
    }

    //Future<String> messageToGet = httpInterface.listenForServersMessage();
    //httpInterface.listenForServersMessage().then((value) => httpInterface.getReturnedMessage().then((value) => addBubble(new Message(false, DateTime.now().toString(), value.toString()))).catchError((error) => print(error.toString())));
    //print("TestReturnedMessage"+httpInterface.getReturnedMessage().toString());
    // httpInterface.getReturnedMessage().then((value) => addBubble(new Message(false, DateTime.now().toString(), value.toString())))
/*
    httpInterface.getReturnedMessage().then((value) => addBubble(new Message(false, DateTime.now().toString(), value.toString())))
        .catchError((error) => print(error.toString()));
*/

/*
    httpInterface.getReturnedMessage().then((value) => addBubble(new Message(false, DateTime.now().toString(), value.toString())))
        .catchError((error) => print(error.toString()));


 */
    //addBubble(new Message(false, DateTime.now().toString(), httpInterface.getReturnedMessage().toString()));
    /*
    messageToGet.then((value) => addBubble(new Message(false, DateTime.now().toString(), value.toString())))
        .catchError((error) => print(error.toString()));

    */
    //print("message to get : " + messageToGet.toString());
    /*
    if(messageToGet != Null) {
      print("display future  : " + messageToGet.toString());
      messageToGet.toString();
      //addBubble();
      //addMessage("test", null,false);
    }
     */

    //messageEventListener.eventBus.fire(MessageEvent(text));
  }

  addHumanBubble(Message message) {
    Bubble bubble;
    //setState(() {
    
      bubble = new Bubble(
        margin: BubbleEdges.only(top: 10),
        radius: Radius.zero,
        alignment: Alignment.topLeft,
        nipWidth: 8,
        nipHeight: 24,
        nip: BubbleNip.leftTop,
        child: Text(message.messageContent, textAlign: TextAlign.left),
      );
    
    bubblesWidgets.add(bubble);
    //});

    return bubble;
    //print(httpInterface.openPersistentConnection().toString()+"testesttestettetet");
  }
  
  addBotBubble(Message message) {
    Bubble bubble;
    //setState(() {
      
        bubble = new Bubble(
          margin: BubbleEdges.only(top: 10),
          radius: Radius.zero,
          alignment: Alignment.topRight,
          nipWidth: 8,
          nipHeight: 24,
          nip: BubbleNip.rightTop,
          color: Color.fromRGBO(225, 255, 199, 1.0),
          child: Text(message.messageContent, textAlign: TextAlign.right),
        );
      
      bubblesWidgets.add(bubble);
    //});

    return bubble;
    //print(httpInterface.openPersistentConnection().toString()+"testesttestettetet");
  }
  /*
  addBubble(Message message) {
    Bubble bubble;
    //setState(() {
    if (message.isHuman) {
      bubble = new Bubble(
        margin: BubbleEdges.only(top: 10),
        radius: Radius.zero,
        alignment: Alignment.topLeft,
        nipWidth: 8,
        nipHeight: 24,
        nip: BubbleNip.leftTop,
        child: Text(message.messageContent, textAlign: TextAlign.left),
      );
    } else {
      bubble = new Bubble(
        margin: BubbleEdges.only(top: 10),
        radius: Radius.zero,
        alignment: Alignment.topRight,
        nipWidth: 8,
        nipHeight: 24,
        nip: BubbleNip.rightTop,
        color: Color.fromRGBO(225, 255, 199, 1.0),
        child: Text(message.messageContent, textAlign: TextAlign.right),
      );
    }
    bubblesWidgets.add(bubble);
    //});

    return bubble;
    //print(httpInterface.openPersistentConnection().toString()+"testesttestettetet");
  }
  */
  
  onClear() {
    addMessage(_textFieldController.text, DateTime.now().toString(),true);
    _textFieldController.text = "";
    /*
    setState(() {
      //print("First text field: ${_textFieldController.text}");
      _textFieldController.text = "";
    });

     */
  }

  /*
  initLogging() {
    // Print output to console.
    Logger.root.onRecord.listen((LogRecord r) {
      print('${r.time}\t${r.loggerName}\t[${r.level.name}]:\t${r.message}');
    });

    // Root logger level.
    Logger.root.level = Level.FINEST;
  }
  */

}