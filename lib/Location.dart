import 'dart:async';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;

class Location {
  String name;
  String site;
  int floor;
  double latitude;
  double longitude;
  var nodeType;

  Location(String name, int floor, String site, double latitude, double longitude, String nodeType) {
    this.name = name;
    this.floor = floor;
    this.site = site;
    this.latitude = latitude;
    this.longitude = longitude;
    this.nodeType = nodeType;
  }

  toString() {
    return "Location{" +
        "name='" + name + '\'' +
        ", floor=" + floor.toString() +
        ", site='" + site + '\'' +
        ", latitude=" + latitude.toString() +
        ", longitude=" + longitude.toString() +
        ", nodeType='" + nodeType + '\'' +
        '}';
  }

  /*
    {
        "id": 10,
        "name": "Balcon Cote Rue"
    }
    */
  /*
  public String toJson(){
    return new JSONObject()
        .put("id", this.getId())
        .put("name", this.getName())
        .toString();
  }

   */


  Map<String, dynamic> toJson() =>
      {
        'name' : name,
        'site' : site,
        'floor' : floor,
        'latitude' : latitude,
        'longitude' : longitude,
        'nodeType' : nodeType,
      };

  Location.fromJson(Map<String, dynamic> jsonParam) :
        name = jsonParam['name'],
        site = jsonParam['site'],
        floor = jsonParam['floor'],
        latitude = jsonParam['latitude'],
        longitude = jsonParam['longitude'],
        nodeType = jsonParam['nodeType'];
/*
  {
    "nodes":[
      {"name":"Entree","id":10},
      {"name":"Fond Chambre 3","id":15},
      {"name":"Balcon Cote Ecole","id":18},
      {"name":"Balcon Cote Rue","id":19}
    ]
  }
*/

/*
  Map<String, dynamic> toJson() =>
      {
        'name' : name,
        'site' : site,
        'floor' : floor,
        'latitude' : latitude,
        'longitude' : longitude,
        'nodeType' : nodeType,
      };

   */

/*
  Location.fromJson(Map<String, dynamic> jsonParam) :
        name = jsonParam['name'],
        site = jsonParam['site'],
        floor = jsonParam['floor'],
        latitude = jsonParam['latitude'],
        longitude = jsonParam['longitude'],
        nodeType = jsonParam['nodeType'];

   */
}

/*
class Location {
  String name;
  String site;
  int floor;
  double latitude;
  double longitude;
  var nodeType;

  Location(String name, int floor, String site, double latitude, double longitude, String nodeType) {
    this.name = name;
    this.floor = floor;
    this.site = site;
    this.latitude = latitude;
    this.longitude = longitude;
    this.nodeType = nodeType;
  }

  toString() {
    return "Location{" +
        "name='" + name + '\'' +
        ", floor=" + floor.toString() +
        ", site='" + site + '\'' +
        ", latitude=" + latitude.toString() +
        ", longitude=" + longitude.toString() +
        ", nodeType='" + nodeType + '\'' +
        '}';
  }
/*
  {
    "nodes":[
      {"name":"Entree","id":10},
      {"name":"Fond Chambre 3","id":15},
      {"name":"Balcon Cote Ecole","id":18},
      {"name":"Balcon Cote Rue","id":19}
    ]
  }
*/
  Map<String, dynamic> toJson() =>
      {
        /*
    'accelerationX' : accelerationX,
    'accelerationY' : accelerationY,
    'accelerationZ' : accelerationZ,
     */
        'name' : name,
        'site' : site,
        'floor' : floor,
        'latitude' : latitude,
        'longitude' : longitude,
        'nodeType' : nodeType,
      };

  Location.fromJson(Map<String, dynamic> jsonParam) :
    name = jsonParam['name'],
    site = jsonParam['site'],
    floor = jsonParam['floor'],
    latitude = jsonParam['latitude'],
    longitude = jsonParam['longitude'],
    nodeType = jsonParam['nodeType'];
}

 */