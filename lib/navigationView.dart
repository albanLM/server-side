import 'package:flutter/material.dart';
import 'NavigationCanvas.dart';
import 'package:permission_handler/permission_handler.dart';

class NavigationViewWidget extends StatefulWidget {
  NavigationViewWidget({Key key}) : super(key: key);
  @override
  _NavigationViewWidgetState createState() => new _NavigationViewWidgetState();
}

class _NavigationViewWidgetState extends State<NavigationViewWidget> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text('Navigation'),
      ),
      backgroundColor: const Color(0xFF424242),
      body:
      new Container(
        child:
        new Padding(
          child:
          new Column(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                new Padding(
                  child:
                  new DropdownButton<String>(
                    onChanged: popupButtonSelected,
                    value: "Child 1",
                    style: new TextStyle(fontSize:19.0,
                        color: const Color(0xFFFFFFFF),
                        fontWeight: FontWeight.w100,
                        fontFamily: "Roboto"),
                    items: <DropdownMenuItem<String>>[
                      const DropdownMenuItem<String>(value: "Child 1",
                          child: const Text("Destination")),
                      const DropdownMenuItem<String>(value: "Child 2",
                          child: const Text("Child 2")),
                      const DropdownMenuItem<String>(value: "Child 3",
                          child: const Text("Child 3")),
                    ],
                  ),

                  padding: const EdgeInsets.all(24.0),
                ),

                new Padding(
                  child:
                  new Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      mainAxisSize: MainAxisSize.max,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        new Text(
                          "Mobilité Réduite : ",
                          style: new TextStyle(fontSize:17.0,
                              color: const Color(0xFFFFFFFF),
                              fontWeight: FontWeight.w100,
                              fontFamily: "Roboto"),
                        ),

                        new Checkbox(key:null, onChanged: checkChanged, value:true)
                      ]

                  ),

                  padding: const EdgeInsets.all(24.0),
                ),

                new Padding(
                  child:
                  //new FlatButton(key:null, onPressed:buttonPressed,
                  new FlatButton(key:null, onPressed:buttonPressed,
                      child:
                      new Text(
                        "Chercher Itinéraire",
                        style: new TextStyle(fontSize:17.0,
                            color: const Color(0xFF000000),
                            fontWeight: FontWeight.w300,
                            fontFamily: "Roboto"),
                      ),
                    color: Colors.greenAccent,
                  ),

                  padding: const EdgeInsets.all(24.0),
                )
              ]

          ),

          padding: const EdgeInsets.all(24.0),
        ),

        padding: const EdgeInsets.all(24.0),
        alignment: Alignment.center,
      ),

    );
  }
  void buttonPressed() {

      // flutter defined function
      showDialog(
        context: context,
        builder: (BuildContext context) {
          // return object of type Dialog
          return AlertDialog(
            backgroundColor: const Color(0xFF424242),
            title: new Text("Oups...",
              style: new TextStyle(color: Colors.white),
            ),
            content: new Text("Aucun itinéraire n'a pu être trouvé selon vos critères",
              style: new TextStyle(color: Colors.white),
            ),
            actions: <Widget>[
              // usually buttons at the bottom of the dialog
              new FlatButton(
                child: new Text("Retour",
                  style: new TextStyle(color: Colors.red),
                ),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    /*
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => NavigationCanvasWidget()),
    );

     */
  }

  void popupButtonSelected(String value) {}

  void checkChanged(bool value){}

}