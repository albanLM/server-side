class StartNavigation {
  double initialPositionX;
  double initialPositionY;
  int initialFloor;
  int destination;
  bool isReducedMobilityFriendly;

  StartNavigation(double initialPositionX, double initialPositionY, int initialFloor, int destination, isReducedMobilityFriendly) {
    this.initialPositionX = initialPositionX;
    this.initialPositionY = initialPositionY;
    this.initialFloor = initialFloor;
    this.destination = destination;
    this.isReducedMobilityFriendly = isReducedMobilityFriendly;
  }



  toString() {
    return "StartNavigation{" +
        "initialPositionX=" + initialPositionX.toString() +
        ", initialPositionY=" + initialPositionY.toString() +
        ", initialFloor=" + initialFloor.toString() +
        ", destination=" + destination.toString() +
        ", isReducedMobilityFriendly='" + isReducedMobilityFriendly.toString() + '\'' +
        '}';
  }

/*
  {
    "initial_position": [
      42.154,
      58.648
    ],
    "initial_floor": 2,
    "destination": 10,
    "isReducedMobilityFriendly": true,
  }
 */

  Map<String, dynamic> toJson() =>
      {
        /*
    'initialPositionX' : initialPositionX,
    'initialPositionY' : initialPositionY,
     */
        'initial_position' : [initialPositionX, initialPositionY],
        'initial_floor' : initialFloor,
        'destination' : destination,
        'isReducedMobilityFriendly' : isReducedMobilityFriendly
      };

  StartNavigation.fromJson(Map<String, dynamic> jsonParam) :
        initialPositionX = jsonParam['initial_position'][0],
        initialPositionY = jsonParam['initial_position'][1],
        initialFloor = jsonParam['initial_floor'],
        destination = jsonParam['destination'],
        isReducedMobilityFriendly = jsonParam['isReducedMobilityFriendly'];

//direction = jsonParam['direction'],
//distance = jsonParam['distance'];
/*
  public void fromJson(String jsonString){
    JSONObject object = new JSONObject(jsonString);
    //JSONObject object = jsonObject.getJSONObject();

    JSONArray initialPosition = object.getJSONArray("initial_position");
    this.setInitialPositionX(initialPosition.getDouble(0));
    this.setInitialPositionY(initialPosition.getDouble(1));

    this.setInitialFloor(object.getInt("initial_floor"));
    this.setDestination(object.getString("destination"));
  }

   */
}