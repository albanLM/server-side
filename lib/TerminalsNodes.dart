import 'dart:async';
import 'dart:convert' as convert;
import 'TerminalNode.dart';

class TerminalsNodes {
  List<TerminalNode> terminalNodesList;

  TerminalsNodes() {
    terminalNodesList = new List<TerminalNode>();
  }

  Map<String, dynamic> toJson() =>
      {
        /*
    'accelerationX' : accelerationX,
    'accelerationY' : accelerationY,
    'accelerationZ' : accelerationZ,
     */
        'nodes' : terminalNodesList,
      };

/*
  TerminalNode.fromJson(Map<String, dynamic> jsonParam) :
        name = jsonParam['name'],
        id = jsonParam['id'];

 */

  TerminalsNodes.fromJson(Map<String, dynamic> jsonParam) {
    // final parsed = convert.jsonDecode(jsonParam.toString()).cast<Map<String, dynamic>>();

    terminalNodesList = jsonParam['nodes'].map<TerminalNode>((json) => TerminalNode.fromJson(json)).toList();
  }

  String toString() {
    return terminalNodesList.toString();
  }

/*
        terminalNodesList = convert.jsonDecode(jsonParam['nodes'])
            .map((data) => TerminalNode.fromJson(data))
            .toList();
  */
/*
        terminalNodesList = convert.jsonDecode(jsonParam['nodes'])
            .map((data) => TerminalNode.fromJson(data))
            .toList();

       */

/*
  {
    "nodes":[
      {"name":"Entree","id":10},
      {"name":"Fond Chambre 3","id":15},
      {"name":"Balcon Cote Ecole","id":18},
      {"name":"Balcon Cote Rue","id":19}
    ]
  }
*/
}