
import 'package:flutter/material.dart';
import 'package:nav_bot/HttpInterface.dart';
import 'package:nav_bot/compass_test.dart';
import 'package:nav_bot/test.dart';
import 'navigationView.dart';

import 'chatbot.dart';

void main() {
  runApp(new MyApp());
}
class MyApp extends StatelessWidget {
/*
  MultiProvider(
  providers: [
  Provider<Something>(create: (_) => Something()),
  Provider<SomethingElse>(create: (_) => SomethingElse()),
  Provider<AnotherThing>(create: (_) => AnotherThing()),
  ],
  child: someWidget,
  )
*/
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'MyCy',
      theme: new ThemeData(
        primarySwatch: Colors.blue,
        primaryColor: const Color(0xFF2196f3),
        accentColor: const Color(0xFF2196f3),
        canvasColor: const Color(0xFFfafafa),
      ),
      home: new MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key}) : super(key: key);
  @override
  _MyHomePageState createState() => new _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      backgroundColor: const Color(0xFF424242),
      appBar: new AppBar(
        title: new Text('MyCy'),
      ),
      body:
      new Center(
        child:
        new Container(
          child:
          new Column(
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                new Padding(
                  child:
                  new FlatButton(key:null, onPressed:navigationButtonPressed,
                      child:
                      new Text(
                        "Navigation",
                        style: new TextStyle(fontSize:20.0,
                            color: const Color(0xFFFFFFFF),
                            fontWeight: FontWeight.bold,
                            fontFamily: "Roboto"),
                      ),
                    color: Colors.blue,
                  ),

                  padding: const EdgeInsets.all(24.0),
                ),

                new Padding(
                  child:
                  new FlatButton(key:null, onPressed:chatbotButtonPressed,
                      child:
                      new Text(
                        "Chatbot",
                        style: new TextStyle(fontSize:20.0,
                            color: const Color(0xFFFFFFFF),
                            fontWeight: FontWeight.bold,
                            fontFamily: "Roboto"),
                      ),
                    color: Colors.orange,
                  ),
                  padding: const EdgeInsets.all(24.0),
                ),

                new Padding(
                  child:
                  new FlatButton(key:null, onPressed:sensorButtonPressed,
                    child:
                    new Text(
                      "Test Capteurs",
                      style: new TextStyle(fontSize:20.0,
                          color: const Color(0xFFFFFFFF),
                          fontWeight: FontWeight.bold,
                          fontFamily: "Roboto"),
                    ),
                    color: Colors.orange,
                  ),
                  padding: const EdgeInsets.all(24.0),
                ),
                new Padding(
                  child:
                  new FlatButton(key:null, onPressed:compassButtonPressed,
                    child:
                    new Text(
                      "Test Compass",
                      style: new TextStyle(fontSize:20.0,
                          color: const Color(0xFFFFFFFF),
                          fontWeight: FontWeight.bold,
                          fontFamily: "Roboto"),
                    ),
                    color: Colors.orange,
                  ),
                  padding: const EdgeInsets.all(24.0),
                )
              ]

          ),

          padding: const EdgeInsets.all(0.0),
          alignment: Alignment.center,
        ),

      ),

    );
  }
  void navigationButtonPressed(){
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => NavigationViewWidget()),
    );
  }

  void chatbotButtonPressed(){
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => ChatbotViewWidget()),
    );
  }

  void sensorButtonPressed(){
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => Test()),
    );
  }

  void compassButtonPressed(){
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => MyCompass()),
    );
  }

}