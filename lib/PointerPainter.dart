
import 'package:flutter/material.dart';
import 'dart:math' as math;
import 'package:vector_math/vector_math.dart' as vector_math;


class PointerPainter extends CustomPainter {
  Animation<double> _animation;
  // Var related to the arrow Rectangle width and height
  double arrowRectWidth;
  double arrowRectHeight;

  // Var of the arrow's side's size
  double arrowTriangleSize;
  double screenWidth;
  double screenHeight;
  double middleX;
  double middleY;

  // Var p
  var painter;

  PointerPainter() {
    painter = Paint()
      ..style = PaintingStyle.fill
      ..strokeWidth = 4.0
      ..color = Colors.redAccent;
  }

  @override
  void paint(Canvas canvas, Size size) {
    // Define a paint object
    arrowRectWidth = size.width/6;
    arrowRectHeight = size.height/5;
    middleX = size.width/2 ;
    middleY = size.height/2;
    double angle = 0;

    canvas.drawPath(getTrianglePath(size.width, size.height,arrowRectWidth, arrowRectHeight), painter,);
    /*
      var path = Path();
      path.moveTo(size.width/2, 0);
      path.lineTo(0, size.height);
      path.lineTo(size.height, size.width);
      path.close();
      canvas.drawPath(path, paint,);
    */

      canvas.drawRRect(
        //RRect.fromRectAndRadius(Rect.fromLTWH(middleX - (arrowRectWidth/2), middleY - (arrowRectHeight/2), arrowRectWidth, arrowRectHeight), Radius.circular(0)),
        RRect.fromRectAndRadius(Rect.fromLTWH(middleOfScreenWithObjectOffset(middleX, arrowRectWidth), middleOfScreenWithObjectOffset(middleY, arrowRectHeight), arrowRectWidth, arrowRectHeight), Radius.circular(0)),
        painter,
      );

      //((direction ?? 0) * (math.pi / 180) * -1
    //angle = 20* (math.pi / 180) * -1;
    angle = vector_math.radians(90);
    canvas.rotate(angle);


  }

  Path getTrianglePath(double screenWidth, double screenHeight, double rectWidth, double rectHeight) {

    double triangleOffsetX = screenWidth/2 - rectWidth/2;
    double triangleOffsetY = screenHeight/2 - rectHeight/2;
    return Path()
      /*
      ..moveTo(0, y)
      ..lineTo(x / 2, 0)
      ..lineTo(x, y)
      ..lineTo(0, y);
       */
      ..moveTo(triangleOffsetX, triangleOffsetY)
      ..lineTo(screenWidth/2, triangleOffsetY-(triangleOffsetY*0.3))
      //..lineTo(screenWidth/2, triangleOffsetY-(triangleOffsetY-(rectHeight/4)))
      //..lineTo(triangleOffsetX+(2*(rectWidth/2)), triangleOffsetY+(2*(rectHeight/2)))
      ..lineTo(triangleOffsetX+rectWidth, triangleOffsetY)
      ..lineTo(triangleOffsetX, triangleOffsetY);
  }

  double middleOfScreenWithObjectOffset(double screenHalfCoordinates, double objectSize) {
    return (screenHalfCoordinates - (objectSize/2));
  }

  @override
  bool shouldRepaint(PointerPainter oldDelegate) => false;
}