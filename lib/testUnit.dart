import 'dart:convert';
import 'NextDirection.dart';
import 'SensorsVectors.dart';
import 'StartNavigation.dart';
import 'TerminalNode.dart';
import 'TerminalsNodes.dart';

void main() {
  /*
    {
        "direction": 130.25,
        "distance": 13.14
    }
  */

  NextDirection nextDirectionTest = new NextDirection(130.25, 13.14);
  print('nextDirectionTest toString : '+nextDirectionTest.toString());
  print(nextDirectionTest.toJson());
  print(jsonEncode(nextDirectionTest).toString());
  print(jsonDecode(jsonEncode(nextDirectionTest)).toString());
  print(NextDirection.fromJson(jsonDecode(jsonEncode(nextDirectionTest))));


  print("====================================");
  /*
  {
    "initial_position": [
      42.154,
      58.648
    ],
    "initial_floor": 2,
    "destination": 10,
    "isReducedMobilityFriendly": true,
  }
 */
  // StartNavigation(double initialPositionX, double initialPositionY, int initialFloor, int destination, isReducedMobilityFriendly)
  StartNavigation startNavigationTest = StartNavigation(42.154, 58.648, 2, 10, true);
  //print(startNavigationTest.destination);
  //print('startNavigationTest toString : '+ startNavigationTest.toString());
  //print(startNavigationTest.toJson());
  print(jsonEncode(startNavigationTest));
  print(StartNavigation.fromJson(jsonDecode(jsonEncode(startNavigationTest))));
  //nextDirectionTest.toJson();
  //print(JsonD);

  /*
    {
        "acceleration": [
            0.154,
            0.648,
            1.151
        ],
        "orientation": 120.02
    }
   */
  print("====================================");
  /*
  {
    "vector":[
      0.25,
      0.65,
      1.21
    ]
  }
   */

  SensorsVectors sensorsVectorsTest = SensorsVectors(0.25, 0.65, 1.21, 120.02);
  //print(startNavigationTest.destination);
  //print('sensorsVectorsTest toString : '+ sensorsVectorsTest.toString());
  //print(sensorsVectorsTest.toJson());
  print(jsonEncode(sensorsVectorsTest).toString());
  //print(jsonDecode(jsonEncode(sensorsVectorsTest)).toString());
  print(SensorsVectors.fromJson(jsonDecode(jsonEncode(sensorsVectorsTest))));

  print("====================================");

  /*
  {
    "nodes":[
      {"name":"Entree","id":10},
      {"name":"Fond Chambre 3","id":15},
      {"name":"Balcon Cote Ecole","id":18},
      {"name":"Balcon Cote Rue","id":19}
    ]
  }
*/

  // SensorsVectors sensorsVectorsTest = SensorsVectors(0.154, 0.648, 1.151, 120.02);
  TerminalNode entree = TerminalNode("Entree", 10);
  TerminalNode fondChambre3  = TerminalNode("Fond Chambre 3", 15);
  TerminalNode balconCoteEcole  = TerminalNode("Balcon Cote Ecole", 18);
  TerminalNode balconCoteRue  = TerminalNode("Balcon Cote Rue", 18);

  TerminalsNodes terminalsNodes = TerminalsNodes();
  terminalsNodes.terminalNodesList.add(entree);
  terminalsNodes.terminalNodesList.add(fondChambre3);
  terminalsNodes.terminalNodesList.add(balconCoteEcole);
  terminalsNodes.terminalNodesList.add(balconCoteRue);

  //print(startNavigationTest.destination);
  print('TerminalsNodes toString : '+ terminalsNodes.toString());
  //print(terminalsNodes.toJson());
  print(jsonEncode(terminalsNodes).toString());
  //print(jsonDecode(jsonEncode(terminalsNodes)).toString());
  print(TerminalsNodes.fromJson(jsonDecode(jsonEncode(terminalsNodes))));
}
