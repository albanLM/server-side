import 'dart:async';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:nav_bot/Message.dart';
import 'dart:convert' as convert;
import 'package:web_socket_channel/io.dart';
import 'package:web_socket_channel/status.dart' as status;
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:permission_handler/permission_handler.dart';


class HttpInterface {
  //var client;
  var channel;
  var webSocketChannel;
  String returnedMessage;

  HttpInterface(typeOfChannel) {
    if (typeOfChannel == "navigation") {
      channel = IOWebSocketChannel.connect('ws://192.168.1.20:10001/ws');
    } else if (typeOfChannel == "chatbot") {
      channel = IOWebSocketChannel.connect('ws://192.168.1.20:10001/ws');
    }
    //client = http.Client();
  }

  initFlutterDownload () async {
    WidgetsFlutterBinding.ensureInitialized();
    await FlutterDownloader.initialize(debug: true);
  }

  bool writeMessageToSocket(Message message) {
    channel.sink.add(message.messageContent);
  }

  downloadFile() async {
    //await Permission.storage.request().isGranted.then((value) async {
    await PermissionHandler().checkPermissionStatus(PermissionGroup.storage).then((value) async {
      var taskId = await FlutterDownloader.enqueue(
        url: 'http://192.168.1.20',
        savedDir: '/storage/emulated/0/Download/',
        fileName: 'requirements.txt',
        showNotification: true, // show download progress in status bar (for Android)
        openFileFromNotification: true, // click on notification to open downloaded file (for Android)
      );
      print("download ???");
      }
    );

    print(await PermissionHandler().checkPermissionStatus(PermissionGroup.storage));
    /*
    if (await PermissionHandler().checkPermissionStatus(PermissionGroup.storage)) {
      // Either the permission was already granted before or the user just granted it.
      final taskId = await FlutterDownloader.enqueue(
        url: 'http://192.168.1.20',
        savedDir: '/storage/emulated/0/Download/',
        fileName: 'requirements.txt',
        showNotification: true, // show download progress in status bar (for Android)
        openFileFromNotification: true, // click on notification to open downloaded file (for Android)
      );
      print("download ???");


    }
     */


  }
}