import 'dart:convert' as convert;
import 'package:vector_math/vector_math.dart';

class SensorsVectors {
  //double accelerationX;
  //double accelerationY;
  //double accelerationZ;
  double orientation;
  Vector3 acceleration;

  SensorsVectors(double accelerationX, double accelerationY, double accelerationZ, double orientation) {
    acceleration = Vector3(accelerationX, accelerationY, accelerationZ);
    //this.accelerationX = accelerationX;
    //this.accelerationY = accelerationY;
    //this.accelerationZ = accelerationZ;
    this.orientation = orientation;
  }

  String toString() {
    return "SensorsVectors{" +
        ', acceleration=' + acceleration.toString() +
        ', orientation=' + orientation.toString() +
        '}';
  }

  Map<String, dynamic> toJson() =>
      {

        'accelerationX' : acceleration.x,
        'accelerationY' : acceleration.y,
        'accelerationZ' : acceleration.z,

        //'acceleration' : [ accelerationX, accelerationY, accelerationZ],
        //'orientation' : orientation,
        //'acceleration' : acceleration.x,
      };

  SensorsVectors.fromJson(Map<String, dynamic> jsonParam) :
        acceleration = Vector3(jsonParam['accelerationX'],jsonParam['accelerationY'],jsonParam['accelerationZ']);
//accelerationX = jsonParam['acceleration'][0],
//accelerationY = jsonParam['acceleration'][1],
//accelerationZ = jsonParam['acceleration'][2],
//orientation = jsonParam['orientation'];
/*
    {
        "acceleration": [
            0.154,
            0.648,
            1.151
        ],
        "orientation": 120.02
    }
    */
/*
  public void fromJson(String jsonString){
    JSONObject object = new JSONObject(jsonString);
    //JSONObject object = jsonObject.getJSONObject("");

    JSONArray acceleration = object.getJSONArray("acceleration");
    this.setAccelerationX(acceleration.getDouble(0));
    this.setAccelerationY(acceleration.getDouble(1));
    this.setAccelerationZ(acceleration.getDouble(2));

    this.setOrientation(object.getDouble("orientation"));
  }

   */
}
