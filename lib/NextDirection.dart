import 'package:flutter/material.dart';
import 'dart:convert' as convert;

class NextDirection {
  double direction;
  double distance;

  NextDirection(double direction, double distance) {
    this.direction = direction;
    this.distance = distance;
  }

  toString() {
    return "NextDirection{" +
        "direction=" + direction.toString() +
        ", distance=" + distance.toString() +
        '}';
  }

  NextDirection.fromJson(Map<String, dynamic> jsonParam) :
        direction = jsonParam['direction'],
        distance = jsonParam['distance'];

  Map<String, dynamic> toJson() =>
      {
        'direction' : direction,
        'distance' : distance,
      };
/*
    {
        "direction": 130.25,
        "distance": 13.14
    }
    */
/*
  fromJson(){
    return new JSONObject()
        .put("direction", this.getDirection())
        .put("distance", this.getDistance())
        .toString();

   */
}